## Short version for non-lawyers

briar-rust is dual-licensed under Apache 2.0 and MIT
terms.

## Longer version

Copyrights in briar-rust are retained by their contributors. No
copyright assignment is required to contribute to briar-rust.

Some files include explicit copyright notices and/or license notices.
For full authorship information, see the version control history.

Except as otherwise noted (below and/or in individual files), briar-rust is
licensed under the [Apache License, Version 2.0](LICENSES/Apache-2.0.txt) or
<http://www.apache.org/licenses/LICENSE-2.0> or the
[MIT license](LICENSES/MIT.txt) or <http://opensource.org/licenses/MIT>, at your option.

briar-rust includes packages written by third parties.
The following third party packages are included, and carry
their own copyright notices and license terms:

* arti. dual-licensed under Apache 2.0 or MIT
* blake2b_simd. licensed under MIT
* clap. dual-licensed under Apache 2.0 or MIT
* data-encoding. licensed under MIT
* data-encoding-macro. licensed under MIT
* env_logger. dual-licensed under Apache 2.0 or MIT
* log. dual-licensed under Apache 2.0 or MIT
* rand_core. dual-licensed under Apache 2.0 or MIT
* salsa20. dual-licensed under Apache 2.0 or MIT
* x25519-dalek. licensed under BSD 3-Clause
