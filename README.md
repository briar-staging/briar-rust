# Briar in Rust

[![docs.rs](https://img.shields.io/docsrs/briar)](https://briar-rust.docs.dorfbrunnen.eu/)

A from-scratch implementation of the
[Bramble Protocols](https://code.briarproject.org/briar/briar-spec) and
[Briar's BSP clients](https://code.briarproject.org/briar/briar/-/wikis/home#clients)
using [Tor's new Rust client arti](https://gitlab.torproject.org/tpo/core/arti)
for use in a new generation of Bramble and Briar clients.

It features a command-line client using nothing but Rust code,
but may get used for other types of clients in the future, too.

In this early stage, this project shall demonstrate how much code
is needed to build a very simple Briar client from scratch.

## Links

* [Official source repository](https://code.briarproject.org/nico/briar-rust)
* [Bramble specification](https://code.briarproject.org/briar/briar-spec/-/tree/master)
* [Clients implemented in Briar](https://code.briarproject.org/briar/briar/-/wikis/home#clients)

## Building and using briar-rust

We expect to be providing official binaries soon.
But, for now, you need to obtain a
[Rust](https://www.rust-lang.org/) development environment,
and build it yourself.

To try it out, compile and run the `briar` binary using the below.

```bash
$ cargo run -p briar --release
```

You can build a binary (but not run it) with:

```
$ cargo build -p briar --release
```

The result can be found as `target/release/briar`.

## Minimum supported Rust Version

Our current Minimum Supported Rust Version (MSRV) is 1.60.

When increasing this MSRV, we won't require any Rust version released in the
last six months. (That is, we'll only require Rust versions released at least
six months ago.)

We will not increase MSRV on PATCH releases, though our dependencies might.

We won't increase MSRV just because we can: we'll only do so when we have a
reason. (We don't guarantee that you'll agree with our reasoning; only that
it will exist.)

## Roadmap

### 0.0.1

* remote contact adding

### 0.0.2

* send and receive private chat messages

## License

briar-rust is primarily distributed under the terms of both the MIT license
and the Apache License (Version 2.0). See [LICENSE](LICENSE.md) for details.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
