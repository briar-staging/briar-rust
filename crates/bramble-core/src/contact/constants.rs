/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Constants around contacts in Bramble.

/// Version of handshake link.
///
/// # References
///
/// * [Specification in BRP](https://code.briarproject.org/briar/briar-spec/-/issues/17)
pub const LINK_VERSION: u8 = 0;

/// Size of handshake link.
///
/// The link consists of 1 byte for [`LINK_VERSION`] and 32 bytes for
/// [`x25519_dalek::PublicKey`].
///
/// # References
///
/// * [Specification in BRP](https://code.briarproject.org/briar/briar-spec/-/issues/17)
pub const LINK_SIZE: usize = 53; // ceil((KEY_SIZE+1)*8/5)
