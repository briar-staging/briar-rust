/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Handling handshake links in Bramble.
//!
//! In Briar, a handshake link looks like, e.g.,
//! briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6.
//!
//! Prefixes like briar:// are added in the respective applications. On this level,
//! a handshake link is always a [`BASE32_NOPAD`]-encoded string of
//! [`LINK_SIZE`].
//!
//! # References
//!
//! * [Specification in BRP](https://code.briarproject.org/briar/briar-spec/-/issues/17)

use crate::contact::constants::{LINK_SIZE, LINK_VERSION};
use crate::crypto::constants::KEY_SIZE;

use data_encoding::BASE32_NOPAD;
use std::fmt;
use x25519_dalek::PublicKey;

/// Encapsulation of handshake link with [`LINK_SIZE`].
///
/// This struct ensures that the link's size is always [`LINK_SIZE`].
pub struct HandshakeLink {
    /// Handshake link without prefixes like, e.g., briar://.
    link: String,
}

impl HandshakeLink {
    /// Extracts [`HandshakeLink`] out of a link with a `prefix`, e.g., "briar".
    ///
    /// # Example
    ///
    /// ```
    /// # use bramble_core::contact::handshake_link::HandshakeLink;
    /// let link = "briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6";
    /// let handshake_link = HandshakeLink::from_prefixed_link(link, "briar");
    /// assert_eq!(
    ///   handshake_link.to_string(),
    ///   "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6"
    /// );
    /// ```
    pub fn from_prefixed_link(link: &str, prefix: &str) -> HandshakeLink {
        link.trim()
            .trim_start_matches(&format!("{}://", prefix))
            .into()
    }

    /// Returns the handshake link with the given `prefix`,
    /// e.g., briar://aab...
    ///
    /// # Example
    ///
    /// ```
    /// # use bramble_core::contact::handshake_link::HandshakeLink;
    /// let link = "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6";
    /// let handshake_link = HandshakeLink::from(link);
    /// assert_eq!(
    ///   handshake_link.with_prefix("briar"),
    ///   "briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6"
    /// );
    /// ```
    pub fn with_prefix(&self, prefix: &str) -> String {
        format!("{}://{}", prefix, self.link)
    }
}

impl fmt::Display for HandshakeLink {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.link)
    }
}

impl From<&str> for HandshakeLink {
    fn from(link: &str) -> HandshakeLink {
        if link.len() != LINK_SIZE {
            panic!("Bramble link must be {} characters long", LINK_SIZE);
        }
        HandshakeLink {
            link: String::from(link),
        }
    }
}

impl From<&PublicKey> for HandshakeLink {
    fn from(public_key: &PublicKey) -> HandshakeLink {
        let mut bytes = public_key.as_bytes().to_vec();
        bytes.insert(0, LINK_VERSION);
        BASE32_NOPAD
            .encode(&bytes)
            .to_ascii_lowercase()
            .as_str()
            .into()
    }
}

impl From<&HandshakeLink> for PublicKey {
    fn from(link: &HandshakeLink) -> PublicKey {
        let mut their_bytes = BASE32_NOPAD
            .decode(link.to_string().to_ascii_uppercase().as_bytes())
            .unwrap();
        let version = their_bytes.drain(0..1).next().unwrap();
        if version != LINK_VERSION {
            panic!("Briar link version {} not supported", version)
        }
        let their_bytes: [u8; KEY_SIZE] = their_bytes.try_into().unwrap();
        PublicKey::from(their_bytes)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn link_from_str() {
        let link = "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6";
        let handshake_link = HandshakeLink::from(link);
        assert_eq!(handshake_link.link, link);
    }

    #[test]
    fn link_from_prefixed_str() {
        let link = "briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6";
        let handshake_link = HandshakeLink::from_prefixed_link(link, "briar");
        assert_eq!(
            handshake_link.link,
            "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6"
        );
    }

    #[test]
    fn link_from_prefixed_str_whitespaces() {
        let link = "    briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6 ";
        let handshake_link = HandshakeLink::from_prefixed_link(link, "briar");
        assert_eq!(
            handshake_link.link,
            "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6"
        );
    }

    #[test]
    #[should_panic]
    fn link_from_short_str() {
        let link = "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w";
        let _ = HandshakeLink::from(link);
    }

    #[test]
    #[should_panic]
    fn link_from_long_str() {
        let link = "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w66";
        let _ = HandshakeLink::from(link);
    }

    #[test]
    #[should_panic]
    fn link_from_briar_link() {
        let link = "briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6";
        let _ = HandshakeLink::from(link);
    }

    #[test]
    fn prefixed_link() {
        let link = HandshakeLink::from("ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6");
        let prefixed = link.with_prefix("briar");
        assert_eq!(
            prefixed,
            "briar://ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6"
        );
    }

    #[test]
    fn display_trait() {
        let link = HandshakeLink::from("ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6");
        assert_eq!(
            format!("{}", link),
            "ab5ujf2haga6y6zqek7vv6hoqadzrwikvo4gtgbtwzvh5qvdn24w6"
        );
    }

    #[test]
    #[ignore]
    fn link_from_public_key() {
        // TODO
    }

    #[test]
    #[should_panic]
    #[ignore]
    fn link_from_invalid_public_key() {
        // TODO
    }

    #[test]
    #[should_panic]
    #[ignore]
    fn link_from_public_key_unsupported_version() {
        // TODO
    }

    #[test]
    #[ignore]
    fn public_key_from_link() {
        // TODO
    }

    #[test]
    #[should_panic]
    #[ignore]
    fn public_key_from_invalid_link() {
        // TODO
    }
}
