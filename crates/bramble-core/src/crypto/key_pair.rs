/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Struct bundling [`StaticSecret`] and [`PublicKey`] together.

use rand_core::OsRng;
use x25519_dalek::{PublicKey, StaticSecret};

/// Bundle of [`StaticSecret`] and [`PublicKey`].
pub struct KeyPair {
    /// Private key of key pair.
    private: StaticSecret,

    /// Public key of key pair, generated out of `private`.
    public: PublicKey,
}

impl KeyPair {
    /// Creates [`KeyPair`] from newly generated [`StaticSecret`].
    pub fn new() -> KeyPair {
        let secret_key = StaticSecret::new(OsRng);
        KeyPair::from(secret_key)
    }

    /// Returns public key.
    pub fn public(&self) -> &PublicKey {
        &self.public
    }

    /// Returns private key.
    pub fn private(&self) -> &StaticSecret {
        &self.private
    }
}

impl From<StaticSecret> for KeyPair {
    fn from(secret_key: StaticSecret) -> KeyPair {
        let public_key = PublicKey::from(&secret_key);
        KeyPair {
            private: secret_key,
            public: public_key,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from() {
        let secret_key = StaticSecret::new(OsRng);
        let public_key = PublicKey::from(&secret_key);
        let key_pair = KeyPair::from(secret_key);
        assert_eq!(key_pair.public, public_key)
    }

    #[test]
    fn public() {
        let secret_key = StaticSecret::new(OsRng);
        let key_pair = KeyPair::from(secret_key);
        assert_eq!(key_pair.public(), &key_pair.public)
    }
}
