/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Constants around cryptography in Bramble.

/// Size of private and public keys.
///
/// # References
///
/// * [BRP Specification](https://code.briarproject.org/briar/briar-spec/-/blob/master/protocols/BRP.md)
/// * [`x25519_dalek::PublicKey`]
pub const KEY_SIZE: usize = 32;
