/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
use std::fs::File;
use std::io::{Read, Write};
use std::net::TcpStream;
use std::process::Command;
use std::str::from_utf8;
use data_encoding::BASE64;
use log::{debug, info};
use sha2::{Digest, Sha512};
use x25519_dalek::StaticSecret;

const DIR: &str = "/home/briar/Documents/briar-rust-data/tor";

pub fn run(private_key: &StaticSecret) {
    start_tor();

    let mut stream = open_control_connection();

    // control-spec.txt, line 1861:
    // For a "ED25519-V3" key is
    //   the Base64 encoding of the concatenation of the 32-byte ed25519 secret
    //   scalar in little-endian and the 32-byte ed25519 PRF secret.

    // generated according to https://www.rfc-editor.org/rfc/rfc8032.html#section-5.1.6
    // assuming that PRF refers to *prefix*
    // inspired by https://docs.rs/ed25519-dalek/latest/src/ed25519_dalek/secret.rs.html#265-279
    let mut h: Sha512 = Sha512::default();
    let mut hash:  [u8; 64] = [0u8; 64];

    h.update(private_key.to_bytes());
    hash.copy_from_slice(h.finalize().as_slice());

    hash[0]  &= 248;
    hash[31] &=  63;
    hash[31] |=  64;

    // generate base64 encoding of hash
    let address = BASE64.encode(&hash);

    let cmd = format!("ADD_ONION ED25519-V3:{} Port=80,127.0.0.1:59050\r\n", address);
    send_cmd(&mut stream, cmd).expect("Adding onion service failed.");
    info!("Successfully added onion service.");

    // infinite waiting to not exit briar-rust -> close control port -> delete hidden service
    // according to https://gitweb.torproject.org/torspec.git/tree/control-spec.txt#n1849
    loop {}
}

fn start_tor() {
    // todo: create torrc dynamically
    let status = Command::new(format!("{}/tor", DIR))
        .arg("-f")
        .arg(format!("{}/torrc", DIR))
        .status()
        .expect("Failed to start Tor daemon");
    info!("Tor daemon started successfully: {}", status.success());
}

fn open_control_connection() -> TcpStream {
    let mut stream = TcpStream::connect("127.0.0.1:59052").expect("Opening control socket failed");

    let mut f = File::open(format!("{}/.tor/control_auth_cookie", DIR)).expect("Auth cookie file doesn't exist");
    let mut buffer = [0; 32];
    let _n = f.read(&mut buffer).expect("Reading auth cookie bytes failed");

    let mut cmd = String::from("AUTHENTICATE ");
    for u in buffer {
        cmd.push_str(&format!("{:02X}", u));
    }
    cmd.push_str("\r\n");

    send_cmd(&mut stream, cmd).expect("Authentication failed.");
    info!("Successfully authenticated.");

    stream
}

fn send_cmd(mut stream: &TcpStream, cmd: String) -> Result<(), &str> {
    debug!("Sending command to tor: {}", cmd);

    match stream.write(cmd.as_ref()) {
        Ok(value) => value,
        Err(_error) => return Err("Writing to stream failed."),
    };
    // todo: parse and return whole response
    let buffer = &mut [0; 8];
    match stream.read(buffer) {
        Ok(value) => value,
        Err(_error) => return Err("Reading from stream failed."),
    };

    if from_utf8(&buffer[0..1]).unwrap() != "2" {
        Err("Authentication failed.")
    } else {
        Ok(())
    }
}
