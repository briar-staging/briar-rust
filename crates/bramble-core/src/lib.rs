/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! A Rust implementation of the Bramble protocols.
//!
//! The communication logic of Briar is handled by a set of protocols called Bramble. This crate
//! provides implementations for all these protocols, including:
//!
//! * [Bramble Handshake Protocol](protocols/bhp)
//! * [Bramble QR Code Protocol](protocols/bqp)
//! * [Bramble Rendezvous Protocol](protocols/brp)
//! * [Bramble Synchronisation Protocol](protocols/bsp)
//! * [Bramble Transport Protocol](protocols/btp)
//!
//! # References
//!
//! * [Specifications of Bramble Protocols](https://code.briarproject.org/briar/briar-spec/)
//! * [Presentation by Nico explaining Briar's internals](https://nico.dorfbrunnen.eu/posts/2021/diving-at-xmpp/)
//! * [Schematic overview of protocols](https://nico.dorfbrunnen.eu/assets/posts/2021/diving-at-xmpp/presentation/#/4/2)

#![allow(rustdoc::private_intra_doc_links)]

use crate::contact::handshake_link::HandshakeLink;
use crate::identity::IdentityManager;
use x25519_dalek::PublicKey;

pub mod contact;
mod crypto;
pub mod identity;
pub mod protocols;
mod tor;

/// Adds a contact with `alias` and `link`.
///
/// This is called "Adding contact at a distance" in Briar clients. This function uses
/// [`protocols::brp`] and [`protocols::bhp`].
pub fn add_contact(identity_manager: &IdentityManager, _alias: &str, link: HandshakeLink) {
    let our_key_pair = identity_manager.key_pair();
    let their_public_key = PublicKey::from(&link);
    protocols::brp::run(our_key_pair, their_public_key);
}
