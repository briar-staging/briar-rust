/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Implementation of the Bramble Rendezvous Protocol (BRP)

use crate::crypto::key_pair::KeyPair;

use blake2b_simd::{Hash, Params};
use log::debug;
use salsa20::cipher::{KeyIvInit, StreamCipher};
use salsa20::Salsa20;
use x25519_dalek::{PublicKey, SharedSecret, StaticSecret};
use crate::tor;

/// Supported BRP version.
const PROTOCOL_VERSION: i8 = 0; // todo: i8 or u8?

pub fn run(our_key_pair: &KeyPair, their_public_key: PublicKey) {
    let our_public_key = our_key_pair.public();
    let our_private_key = our_key_pair.private();
    let is_alice = check_if_alice(our_public_key, &their_public_key);

    // calculate raw shared secret
    let raw_secret = our_private_key.diffie_hellman(&their_public_key);
    debug!("{:?}", raw_secret.as_bytes());
    if secret_is_zero(&raw_secret) {
        // abort BRP if the raw shared secret is all zeros
        return;
    }

    // derive "cooked" shared secret
    let static_master_key = derive_cooked_shared_secret(their_public_key, our_public_key, is_alice, &raw_secret);
    debug!("{:?}", static_master_key.as_bytes());

    // derive rendezvous key
    let rendezvous_key = kdf(
        static_master_key.as_bytes(),
        &[
            "org.briarproject.bramble.rendezvous/RENDEZVOUS_KEY".as_bytes(),
            &PROTOCOL_VERSION.to_le_bytes(), // todo: le or be?
        ],
    );
    debug!("{:?}", rendezvous_key.as_bytes());

    // derive stream key
    let transport_id = "org.briarproject.bramble.tor";
    let stream_key = kdf(
        rendezvous_key.as_bytes(),
        &[
            "org.briarproject.bramble.rendezvous/KEY_MATERIAL".as_bytes(),
            transport_id.as_bytes(),
        ],
    );
    debug!("{:?}", stream_key.as_bytes());

    // derive key material
    let nonce = [0u8; 8];
    let mut cipher = Salsa20::new(stream_key.as_bytes().into(), &nonce.into());
    let mut buffer = [0u8; 64];
    cipher.apply_keystream(&mut buffer);
    let key_material = buffer.clone();
    debug!("{:?}", key_material);

    // derive key pairs for both peers
    let (seed_a, seed_b) = key_material.split_at(32);
    let key_pair_a = derive_key_pair(seed_a);
    let key_pair_b = derive_key_pair(seed_b);

    let my_private_key = if is_alice {
        key_pair_a.private()
    } else {
        key_pair_b.private()
    };
    tor::process::run(&my_private_key);

    // TODO: Connect to other Hidden Service, see Java implementation:
    // https://code.briarproject.org/briar/briar/-/blob/release-1.4.20/bramble-core/src/main/java/org/briarproject/bramble/crypto/CryptoComponentImpl.java#L461
}

fn derive_cooked_shared_secret(their_public_key: PublicKey, our_public_key: &PublicKey, is_alice: bool, raw_secret: &SharedSecret) -> Hash {
    let (pub_a, pub_b) = if is_alice {
        (our_public_key.as_bytes(), their_public_key.as_bytes())
    } else {
        (their_public_key.as_bytes(), our_public_key.as_bytes())
    };
    let static_master_key = hash(&[
        "org.briarproject.bramble.transport/STATIC_MASTER_KEY".as_bytes(),
        raw_secret.as_bytes(),
        pub_a,
        pub_b,
    ]);
    debug!("{:?}", static_master_key.as_bytes());
    static_master_key
}

fn derive_key_pair(seed: &[u8]) -> KeyPair {
    let seed: &[u8; 32] = seed.try_into().unwrap();
    let secret = StaticSecret::from(*seed);
    KeyPair::from(secret)
}

fn secret_is_zero(raw_secret: &SharedSecret) -> bool {
    raw_secret.as_bytes().iter().all(|b| *b == 0)
}

fn check_if_alice(our: &PublicKey, their: &PublicKey) -> bool {
    our.as_bytes() < their.as_bytes()
}

// todo: consider using a macro instead to concatenate x_i
fn concat(x: &[&[u8]]) -> Vec<u8> {
    let mut vec = Vec::new();
    for x_i in x {
        vec.extend_from_slice(&(x_i.len() as i32).to_le_bytes()); // todo: i32 or u32? le or be?
        vec.extend_from_slice(x_i)
    }
    vec
}

fn hash(x: &[&[u8]]) -> Hash {
    Params::default().hash_length(32).hash(&concat(x))
}

fn kdf(k: &[u8], x: &[&[u8]]) -> Hash {
    Params::default().hash_length(32).key(k).hash(&concat(x))
}

#[cfg(test)]
mod tests {
    use super::*;

    // https://code.briarproject.org/briar/briar/-/blob/release-1.4.20/bramble-core/src/test/java/org/briarproject/bramble/crypto/KeyAgreementTest.java
    // https://code.briarproject.org/briar/briar/-/blob/release-1.4.20/bramble-core/src/test/java/org/briarproject/bramble/crypto/KeyEncodingAndParsingTest.java

    #[test]
    #[ignore]
    fn derive_shared_secret() {
        // TODO
    }

    #[test]
    #[should_panic]
    #[ignore]
    fn onion_from_handshake_links() {
        // TODO
    }
}

