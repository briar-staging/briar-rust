/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Managing identity in Bramble.

use crate::contact::handshake_link::HandshakeLink;
use crate::crypto::key_pair::KeyPair;

/// Collection of identity properties.
pub struct IdentityManager {
    key_pair: KeyPair,
    link: HandshakeLink,
}

impl IdentityManager {
    pub fn new() -> IdentityManager {
        let key_pair = KeyPair::new();
        let link = HandshakeLink::from(key_pair.public());
        IdentityManager { key_pair, link }
    }

    /// Returns [`KeyPair`] of [`IdentityManager`].
    pub fn key_pair(&self) -> &KeyPair {
        &self.key_pair
    }

    /// Returns [`HandshakeLink`] of [`IdentityManager`].
    pub fn handshake_link(&self) -> &HandshakeLink {
        &self.link
    }
}
