/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! A command-line client for connecting to the Briar network.
//!
//! For using Briar as a library, use [`briar_client`] instead.

use clap::Parser;
use env_logger::Env;
use log::info;

use briar_client;

/// Arguments to be passed via [`clap`].
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Alias of your contact
    #[arg(short, long, default_value_t = String::from("Alice"))]
    alias: String,

    /// briar:// link of your contact
    #[arg(short, long, default_value_t = String::from("briar://ab7iukvowmrrlcdmcooiax6sj5b4su6fa5shpwvlww2ocawyf3cu2"))]
    contact: String,
}

/// Starts the command-line client with the arguments passed via [`clap`].
pub fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();
    let args = Args::parse();

    info!("Will connect to {} at {}", args.alias, args.contact);

    briar_client::main(&args.alias, &args.contact);
}
