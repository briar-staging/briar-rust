/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! Library for using Briar.
//!
//! It's built upon [`briar_core`] and [`bramble_core`], providing a user-friendly stable API.

use log::info;

use bramble_core::contact::handshake_link::HandshakeLink;
use bramble_core::identity::IdentityManager;

/// Starts Briar and directly adds a contact with `alias` and the briar:// link `link`.
pub fn main(alias: &str, link: &str) {
    let identity_manager = IdentityManager::new();

    let our_link = identity_manager.handshake_link();
    info!("Our link: {}", our_link.with_prefix("briar"));

    let their_link = HandshakeLink::from_prefixed_link(link, "briar");

    bramble_core::add_contact(&identity_manager, alias, their_link);
}
