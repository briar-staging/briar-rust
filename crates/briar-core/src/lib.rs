/*
 * SPDX-FileCopyrightText: 2023 The Briar Project <contact@briarproject.org>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
//! A Rust implementation of the Briar protocols
//!
//! It's built upon [`bramble_core`], featuring an implementation of Briar's
//! Bramble Synchronisation Protocol (BSP) clients.
//!
//! # References
//!
//! * [Briar's BSP clients](https://code.briarproject.org/briar/briar/-/wikis/home#clients)
